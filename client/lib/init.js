const Web3 = require('web3');

// check if web3 has been injected from MetaMask (or similar)
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    let providerUrl = Meteor.settings.public.ethereumHost + ":" + Meteor.settings.public.ethereumPort;
    web3 = new Web3(new Web3.providers.HttpProvider("http://" + providerUrl));
}

// set default account based on settings
let ethereumAccount = Meteor.settings.public.ethereumAccount;
if (typeof ethereumAccount == "string") {
    // if setting is a string, then it is an account address
    web3.defaultAccount = ethereumAccount;
} else {
    // if setting is not a string, then it is an account index
    web3.defaultAccount = web3.eth.accounts[ethereumAccount];
}

// set global account to use for interacting with the chain
Session.set("web3Account", web3.defaultAccount);

/*
 * This script sets a static account to use throughout the application
 * In a full application it should be expanded to allow a user to switch account
 * A package is available to help with this: https://github.com/ethereum/meteor-package-accounts
 */
