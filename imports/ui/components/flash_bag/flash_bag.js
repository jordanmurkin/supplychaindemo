import './flash_bag.html';
import './flash_bag.css';

import { FlashBag } from '../../../lib/FlashBag.js';

Template.flash.created = function () {
    // Get the ID of the messsage
    let id = this.data.id;
    Meteor.setTimeout(function () {
        // delete the flash message after 2 seconds
        FlashBag.remove(id);
    }, 2000);
}

Template.flash.helpers({
    isSuccessMessage(type) {
        return type == "success";
    }
});
