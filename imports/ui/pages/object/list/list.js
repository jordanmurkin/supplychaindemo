/* global Template */
/* jshint esversion:6 */

import './list.html';
import './list.css';

import { ObjectManager } from "../../../../lib/ObjectManager.js";

Template.ObjectListPage.created = function() {
    let self = this;

    self.objects = new ReactiveVar([]);

    ObjectManager.getObjects(Session.get("web3Account"))
    .then(objects => {
        self.objects.set(objects);
    })
    .catch(error => {
        console.log(error);
    })
}

Template.ObjectListPage.helpers({
    objects() {
        return Template.instance().objects.get();
    }
});

Template.ObjectListPage.events({

});
