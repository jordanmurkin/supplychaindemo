/* global Template */
/* jshint esversion:6 */

import './view.html';
import './view.css';

import { ObjectManager } from "../../../../lib/ObjectManager.js";
import { Object } from "../../../../lib/Object.js";
import { FlashBag } from "../../../../lib/FlashBag.js";

Template.ObjectViewPage.created = function() {
    let self = this;
    const params = this.data.params();

    if (typeof params.identifier == "undefined") {
        // TODO error handling
    }

    self.identifier = params.identifier;
    self.owner = new ReactiveVar(null);
    self.address = new ReactiveVar(null);
    self.historyLogs = new ReactiveVar([]);
    self.historyWatch = null;

    ObjectManager.getObject(self.identifier)
    .then(objectAddress => {
        self.address.set(objectAddress);

        let object = new Object(objectAddress);

        return object.getOwner();
    })
    .then(owner => {
        self.owner.set(owner);
    })
    .catch(error => {
        console.log(error);
    });

    refreshHistory(this);
}

Template.ObjectViewPage.helpers({
    identifier() {
        return Template.instance().identifier;
    },
    address() {
        return Template.instance().address.get();
    },
    owner() {
        return Template.instance().owner.get();
    },
    isOwner() {
        return Template.instance().owner.get() == Session.get('web3Account');
    },
    historyLogs() {
        return Template.instance().historyLogs.get();
    }
});

Template.ObjectViewPage.events({
    'submit #transferObjectForm'(event, tmpl) {
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const address = target.address.value;

        ObjectManager.transferObject(tmpl.identifier, address)
        .then(success => {
            FlashBag.success("Object transferred");
        })
        .catch(error => {
            FlashBag.error(error.message);
        });
    }
});

Template.ObjectViewPage.destroyed = function() {
    if (this.historyWatch != null) {
        this.historyWatch.stopWatching();   
    }
}

function refreshHistory(tmpl) {
    ObjectManager.getObjectTransfers(tmpl.identifier)
    .then(historyLogs => {
        tmpl.historyLogs.set(historyLogs);
        
        return ObjectManager.getObjectTransferEvent();
    })
    .then(event => {
        tmpl.historyWatch = event;
        
        event.watch(function(error, result) {
            if (!error) {
                console.log(result);
                
                tmpl.historyLogs.push({
                    "from": result.args.from,
                    "to": result.args.to,
                    "block": result.blockNumber
                });
            }
        });
    })
    .catch(error => {
        console.log(error);
    });
}
