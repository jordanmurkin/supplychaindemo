/* global Template */
/* jshint esversion:6 */

import './add.html';
import './add.css';

import { ObjectManager } from "../../../../lib/ObjectManager.js";
import { FlashBag } from '../../../../lib/FlashBag.js';

Template.ObjectAddPage.created = function() {

}

Template.ObjectAddPage.helpers({

});

Template.ObjectAddPage.events({
    'submit #registerObjectForm'(event, tmpl) {
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const identifier = target.identifier.value;
        const ownerAddress = target.ownerAddress.value;

        ObjectManager.isObjectRegistered(identifier)
        .then(isRegistered => {
            if (isRegistered) {
                throw new Error("Object is already registered");
            }

            ObjectManager.registerObject(identifier, ownerAddress);
        })
        .then(success => {
            FlashBag.success("Object registered");
        })
        .catch(error => {
            FlashBag.error(error.message);
        });
    }
});
