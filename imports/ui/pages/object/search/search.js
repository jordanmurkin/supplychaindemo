/* global Template */
/* jshint esversion:6 */

import './search.html';
import './search.css';

import { ObjectManager } from "../../../../lib/ObjectManager.js";
import { Object } from "../../../../lib/Object.js";
import { FlashBag } from "../../../../lib/FlashBag.js";

Template.ObjectSearchPage.created = function() {
    this.objectFound = new ReactiveVar(false);
    this.objectOwner = new ReactiveVar(null);
    this.objectAddress = new ReactiveVar(null);
    this.objectHistory = new ReactiveVar(null);
}

Template.ObjectSearchPage.helpers({
    objectFound() {
        return Template.instance().objectFound.get();
    },
    objectOwner() {
        return Template.instance().objectOwner.get();
    },
    objectAddress() {
        return Template.instance().objectAddress.get();
    },
    objectHistory() {
        return Template.instance().objectHistory.get();
    }
});

Template.ObjectSearchPage.events({
    'submit #searchObjectForm'(event, tmpl) {
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const search = target.search.value;

        ObjectManager.isObjectRegistered(search)
        .then(isRegistered => {
            if (!isRegistered) {
                throw new Error("Object is not registered");
            }

            return ObjectManager.getObject(search);
        })
        .then(object => {
            updateObject(search, object, tmpl);
        })
        .catch(error => {
            tmpl.objectFound.set(false);

            FlashBag.error(error.message);
        });
    }
});

function updateObject(identifier, objectAddress, tmpl) {
    tmpl.objectFound.set(true);
    tmpl.objectAddress.set(objectAddress);

    let object = new Object(objectAddress);

    object.getOwner()
    .then(owner => {
        tmpl.objectOwner.set(owner);
    });

    ObjectManager.getObjectTransfers(identifier)
    .then(historyLogs => {
        console.log(historyLogs);

        tmpl.objectHistory.set(historyLogs);
    })
    .catch(error => {
        console.log(error);
    });
}
