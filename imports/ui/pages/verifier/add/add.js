/* global Template */
/* jshint esversion:6 */

import './add.html';
import './add.css';

import { ObjectManager } from "../../../../lib/ObjectManager.js";
import { FlashBag } from '../../../../lib/FlashBag.js';

Template.VerifierAddPage.created = function () {

};

Template.VerifierAddPage.events({
    'submit #addVerifierForm'(event, tmpl) {
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const address = target.address.value;

        ObjectManager.isVerifier(address)
        .then(isVerifier => {
            if (isVerifier) {
                throw new Error("Verifier already added");
            }

            return ObjectManager.addVerifier(address);
        })
        .then(success => {
            FlashBag.success("Verifier added");
        })
        .catch(error => {
            FlashBag.error(error.message);
        });
    }
});
