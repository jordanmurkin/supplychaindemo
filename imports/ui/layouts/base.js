/* global Template */
/* jshint esversion:6 */

import './base.html';
import './base.css';

import { ObjectManager } from "../../lib/ObjectManager.js";

Template.baseLayout.created = function() {
    let self = this;

    self.isObjectManagerVerifier = new ReactiveVar(false);
    self.isObjectManagerOwner = new ReactiveVar(false);

    ObjectManager.isVerifier(web3.defaultAccount)
    .then(isObjectVerifier => {
        self.isObjectManagerVerifier.set(isObjectVerifier);
    })
    .catch(error => {
        console.log(error);
    });

    ObjectManager.isOwner(web3.defaultAccount)
    .then(isOwner => {
        self.isObjectManagerOwner.set(isOwner);
    })
    .catch(error => {
        console.log(error);
    });
};

Template.baseLayout.helpers({
    isObjectManagerVerifier() {
        return Template.instance().isObjectManagerVerifier.get();
    },
    isObjectManagerOwner() {
        return Template.instance().isObjectManagerOwner.get();
    }
});
