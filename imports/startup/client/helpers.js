Template.registerHelper('web3Account', function () {
    return Session.get("web3Account");
});

Template.registerHelper('truncate', function (s, l) {
    return s.substr(0, l);
});

Template.registerHelper('toHex', function (s) {
    return web3.toHex(s);
});

Template.registerHelper('fromHex', function (h) {
    return web3.toAscii(h);
});

Template.registerHelper('flashBag', function () {
    return Session.get("flashBag");
});
