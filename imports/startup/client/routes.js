/* jshint esversion:6 */

import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// import base templates
import '../../ui/layouts/base.js';

// import application pages
import '../../ui/pages/verifier/add/add.js';
import '../../ui/pages/object/list/list.js';
import '../../ui/pages/object/search/search.js';
import '../../ui/pages/object/add/add.js';
import '../../ui/pages/object/view/view.js';

// import components
import '../../ui/components/flash_bag/flash_bag.js';

// route definitions
FlowRouter.route('/', {
    name: 'Object.list',
    action() {
        BlazeLayout.render('baseLayout', {
            main: 'ObjectListPage'
        });
    },
});

FlowRouter.route('/object/search', {
    name: 'Object.search',
    action() {
        BlazeLayout.render('baseLayout', {
            main: 'ObjectSearchPage'
        });
    },
});

FlowRouter.route('/object/add', {
    name: 'Object.add',
    action() {
        BlazeLayout.render('baseLayout', {
            main: 'ObjectAddPage'
        });
    },
});

FlowRouter.route('/object/view/:identifier', {
    name: 'Object.view',
    action(params) {
        BlazeLayout.render('baseLayout', {
            main: 'ObjectViewPage',
            params: params
        });
    },
});

FlowRouter.route('/verifier/add', {
   name: 'Verifier.add',
   action() {
       BlazeLayout.render('baseLayout', {
           main: 'VerifierAddPage'
       });
   },
});
