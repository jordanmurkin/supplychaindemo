/* jshint esversion:6 */
/* global module, require */

// ObjectManager.js
//
// Author: Jordan Murkin
// Created: 29 January 2018

import contract from "truffle-contract";

class objectManager {
    constructor() {
        const objectManagerData = require("./truffle/build/contracts/ObjectManager.json");

        let objectManagerContract = contract(objectManagerData);
        objectManagerContract.setProvider(web3.currentProvider);

        this.contract = objectManagerContract;
    }

    getOwner() {
        let self = this;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(instance => {
                return instance.owner();
            })
            .then(owner => {
                resolve(owner);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    isOwner(address) {
        let self = this;

        return new Promise(function(resolve, reject) {
            self.getOwner()
            .then(owner => {
                resolve(owner == address);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    /** Verifiers **/

    isVerifier(address) {
        let self = this;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(instance => {
                return instance.verifiers(address);
            })
            .then(isVerifier => {
                resolve(isVerifier);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    addVerifier(address) {
        let self = this,
            instance = null;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                return instance.addVerifier.estimateGas(address, {from: Session.get('web3Account')});
            })
            .then(gasEstimate => {
                return instance.addVerifier(address, {from: Session.get('web3Account'), gas: gasEstimate});
            })
            .then(success => {
                resolve(success);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    removeVerifier(address) {
        let self = this,
            instance = null;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                return instance.removeVerifier.estimateGas(address, {from: Session.get('web3Account')});
            })
            .then(gasEstimate => {
                return instance.removeVerifier(address, {from: Session.get('web3Account'), gas: gasEstimate});
            })
            .then(success => {
                resolve(success);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    /** Objects **/

    getObject(identifier) {
        let self = this;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(instance => {
                return instance.objects(identifier);
            })
            .then(object => {
                resolve(object);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    isObjectRegistered(identifier) {
        let self = this;

        return new Promise(function(resolve, reject) {
            self.getObject(identifier)
            .then(object => {
                resolve(object != "0x0000000000000000000000000000000000000000");
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getObjects(address) {
        let self = this,
            instance = null;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                return instance.objectOwnersIndex(address);
            })
            .then(index => {
                let promises = [];

                for (let i = 0; i < index; i++) {
                    promises.push(instance.objectOwners(address, i));
                }

                resolve(Promise.all(promises));
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    registerObject(identifier, owner) {
        let self = this,
            instance = null;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                return instance.registerObject.estimateGas(identifier, owner, {from: Session.get('web3Account')});
            })
            .then(gasEstimate => {
                return instance.registerObject(identifier, owner, {from: Session.get('web3Account'), gas: gasEstimate});
            })
            .then(success => {
                resolve(success);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    transferObject(identifier, newOwner) {
        let self = this,
            instance = null;

        console.log("Transfer step 1");

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                console.log("Transfer step 2");

                return instance.transferObject.estimateGas(identifier, newOwner, {from: Session.get('web3Account')});
            })
            .then(gasEstimate => {
                console.log("Transfer step 3");

                return instance.transferObject(identifier, newOwner, {from: Session.get('web3Account'), gas: gasEstimate});
            })
            .then(success => {
                resolve(success);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getObjectTransfers(object) {
        let self = this,
            instance = null;

        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                instance = deployed;

                resolve(self.getObjectTransfersEvents(object, instance));
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getObjectTransfersEvents(object, contract) {
        return new Promise(function(resolve, reject) {
            contract.ObjectTransferred({"object": object}, {fromBlock: 0, toBlock: 'latest' }).get(function(error, result) {
                if (error) {
                    reject(error);
                } else {
                    let historyLogs = [];

                    for (let i = 0; i < result.length; i++) {
                        historyLogs.push({
                            "from": result[i].args.from,
                            "to": result[i].args.to,
                            "block": result[i].blockNumber
                        });
                    }

                    resolve(historyLogs);
                }
            });
        });
    }
    
    getObjectTransferEvent(object) {
        let self = this;
        
        return new Promise(function(resolve, reject) {
            self.contract.deployed()
            .then(deployed => {
                resolve(deployed.ObjectTransferred({"object": object}, {fromBlock: 'pending'}));
            })
            .catch(error => {
                reject(error);
            });
        });
    }
}

export let ObjectManager = new objectManager();
