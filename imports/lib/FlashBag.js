/* jshint esversion:6 */
/* global module, require */

// FlashBag.js
//
// Author: Jordan Murkin
// Created: 21 October 2017

export class FlashBag {
    static add(message, type) {
        let flashes = Session.get('flashBag') || [];

        let id = new Date().getTime();

        let flash = {
            id: id,
            message: message,
            type: type
        }

        flashes.push(flash);

        Session.set('flashBag', flashes);
    }

    static success(message) {
        this.add(message, "success");
    }

    static error(message) {
        this.add(message, "error");
    }

    static remove(id) {
        let flashes = Session.get('flashBag') || [];

        flashes.forEach(function(flash, i) {
            if (flash.id == id) {
                flashes.splice(i, 1);
                return;
            }
        });

        Session.set('flashBag', flashes);
    }

    static clear() {
        Session.set('flashBag', []);
    }
}
