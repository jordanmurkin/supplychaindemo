module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    networks: {
        development: {
            host: "163.83.136.139",
            port: 8545,
            network_id: "*", // Match any network id
            gas: 3000000
        }
    }
};
