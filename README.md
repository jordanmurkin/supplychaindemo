# Supply Chain Application Tutorial
This application has been created as a tutorial for the Blockathon events

# Installation
1. Clone the project
2. In the project directory run `meteor npm install`
3. You can now type `npm start` to run the project

Author: Jordan Murkin
Date created: 30 January 2018
